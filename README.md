# Projeto do Desafio React Native - ioasys

Este é o repositório com o projeto criado para o desafio de desenvolvimento em React Native. Abaixo são mostradas as telas do aplicativo desenvolvido:

![alt text](screens.png)

## Instruções de uso

Clonar este repositório e instalar suas dependências:

```
  $ yarn
```

Criar um arquivo `.env` no diretório raiz e adicionar a url base da api:

```
  $ API_URL='url_base'
```

Para executar um simulador iOS:

```
  $ react-native run-ios
```

Para executar um simulador Android:

```
  $ react-native run-android
```

## Bibliotecas utilizadas

- `axios`: responsável por fazer requisições à API.
- `prop-types`: auxilia na verificação das props passadas a cada componente.
- `react` e `react-native`: principais bibliotecas da aplicação, são a base para seu funcionamento.
- `react-native-dotenv`: permite a utilização de variáveis ambiente configuradas em arquivos do tipo `.env`.
- `react-native-flash-message`: exibição de mensagens de feedback, como as mensagens de erro no topo da tela.
- `react-navigation` e `react-native-gesture-handler`: responsáveis pela navegação dentro do aplicativo.
- `react-native-status-bar-height`: helper para obter a altura da status bar em diferentes dispositivos.
- `react-native-vector-icons`: permite a utilização de ícones na aplicação. O pacote utilizado neste caso foi o FontAwesome 5.
- `redux` e `react-redux`: necessárias para a configuração e utilização do Redux na aplicação.
- `redux-saga`: necessária para a configuração do Redux Saga na aplicação.
- `reactotron-react-native`: responsável por integrar a aplicação com o Reactotron.
- `@babel/core`, `@babel/runtime`, `babel-jest`, `metro-react-native-babel-preset`: bibliotecas para configuração do Babel
- `babel-eslint`: biblioteca para adicionar ao eslint o código entendido pelo Babel.
- `eslint`, `eslint-config-airbnb`, `eslint-plugin-import`, `eslint-plugin-jsx-a11y`, `eslint-plugin-react`: bibliotecas para configuração do ESLint.
