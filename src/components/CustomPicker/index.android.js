import React, { Component } from 'react';
import { Picker } from 'react-native';
import PropTypes from 'prop-types';

import styles from './styles';

export default class CustomPicker extends Component {
  static propTypes = {
    setSelectedValue: PropTypes.func.isRequired,
    list: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number,
        enterprise_type_name: PropTypes.string,
      }),
    ).isRequired,
    selectedValue: PropTypes.number.isRequired,
  };

  state = {};

  render() {
    const { setSelectedValue, list, selectedValue } = this.props;
    return (
      <Picker
        selectedValue={selectedValue}
        style={styles.picker}
        onValueChange={itemValue => setSelectedValue(itemValue)}
      >
        {list.map(item => (
          <Picker.Item key={item.id} label={item.enterprise_type_name} value={item.id} />
        ))}
      </Picker>
    );
  }
}
