import React, { Component } from 'react';
import {
  Modal, Picker, Text, TouchableOpacity, View,
} from 'react-native';
import PropTypes from 'prop-types';

import styles from './styles';

export default class CustomPicker extends Component {
  static propTypes = {
    setSelectedValue: PropTypes.func.isRequired,
    description: PropTypes.string.isRequired,
    list: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number,
        enterprise_type_name: PropTypes.string,
      }),
    ).isRequired,
  };

  state = {
    modalVisible: false,
    title: 'Todos os tipos',
    selectedValue: -1,
  };

  setModalVisibility(visibility) {
    this.setState({ modalVisible: visibility });
  }

  updateSelectedValue = (value, type) => {
    this.setState({ selectedValue: value, title: type });
  };

  closeModal = () => {
    const { setSelectedValue } = this.props;
    const { selectedValue } = this.state;

    setSelectedValue(selectedValue);
    this.setModalVisibility(false);
  };

  render() {
    const { modalVisible, selectedValue, title } = this.state;
    const { description, list } = this.props;
    return (
      <View style={styles.pickerContainer}>
        <Modal
          animationType="fade"
          transparent={false}
          visible={modalVisible}
          onRequestClose={() => {}}
        >
          <View style={styles.backgroundContainer}>
            <View style={styles.modalContainer}>
              <View style={styles.center}>
                <Text style={styles.description}>{description}</Text>
              </View>
              <Picker
                selectedValue={selectedValue}
                onValueChange={(value, index) => this.updateSelectedValue(value, list[index].enterprise_type_name)
                }
                style={styles.picker}
              >
                {list.map(item => (
                  <Picker.Item key={item.id} label={item.enterprise_type_name} value={item.id} />
                ))}
              </Picker>

              <View style={styles.center}>
                <TouchableOpacity onPress={this.closeModal} style={styles.selectButton}>
                  <Text style={styles.buttonText}>Selecionar</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>

        <TouchableOpacity
          onPress={() => {
            this.setModalVisibility(true);
          }}
          style={styles.pickerButton}
        >
          <Text style={styles.pickerButtonText}>{title}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
