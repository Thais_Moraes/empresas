import { StyleSheet } from 'react-native';
import { metrics } from '../../styles';

const styles = StyleSheet.create({
  picker: {
    width: 0.45 * (metrics.screenWidth - metrics.basePadding * 2),
  },
});

export default styles;
