import { StyleSheet } from 'react-native';
import { colors, metrics } from '../../styles';

const styles = StyleSheet.create({
  backgroundContainer: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: colors.primary,
    padding: metrics.basePadding,
  },

  pickerContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    backgroundColor: colors.lighter,
    borderRadius: metrics.baseRadius,
    paddingHorizontal: metrics.basePadding,
  },

  picker: {
    marginVertical: metrics.baseMargin,
  },

  modalContainer: {
    justifyContent: 'center',
    backgroundColor: colors.white,
    padding: metrics.basePadding * 2,
    borderRadius: metrics.baseRadius,
  },

  selectButton: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.primary,
    padding: metrics.basePadding / 2,
    borderRadius: metrics.baseRadius,
    marginTop: metrics.baseMargin,
  },

  buttonText: {
    color: colors.white,
    fontSize: 16,
    fontWeight: 'bold',
  },

  center: {
    flexDirection: 'row',
    justifyContent: 'center',
  },

  description: {
    fontSize: 16,
    textAlign: 'center',
  },

  pickerButton: {},

  pickerButtonText: {
    fontSize: 14,
    color: colors.dark,
  },
});

export default styles;
