import React, { Component } from 'react';
import {
  StatusBar, Text, TouchableOpacity, View,
} from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withNavigation } from 'react-navigation';
import PropTypes from 'prop-types';
import styles from './styles';

import * as signOutActions from '../../store/actions/signout';

class Header extends Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
    signOutRequest: PropTypes.func.isRequired,
  };

  signOut = () => {
    const { signOutRequest } = this.props;
    signOutRequest();
  };

  render() {
    const { title } = this.props;
    return (
      <View style={styles.container}>
        <StatusBar barStyle="light-content" />
        <View style={styles.left} />
        <Text style={styles.title}>{title}</Text>
        <TouchableOpacity onPress={this.signOut} style={styles.exitContainer}>
          <Text style={styles.exitText}>Sair</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const mapDispatchToProps = dispatch => bindActionCreators(signOutActions, dispatch);

export default withNavigation(
  connect(
    null,
    mapDispatchToProps,
  )(Header),
);
