import { StyleSheet } from 'react-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';

import { colors, metrics } from '../../styles';

const styles = StyleSheet.create({
  container: {
    height: 54 + getStatusBarHeight(true),
    paddingTop: getStatusBarHeight(true),
    borderBottomWidth: 1,
    borderBottomColor: colors.light,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: metrics.basePadding,
    backgroundColor: colors.primary,
  },

  title: {
    fontSize: 16,
    fontWeight: '300',
    color: colors.white,
  },

  exitContainer: {
    flexDirection: 'row',
  },

  exitText: {
    fontSize: 14,
    fontWeight: '300',
    color: colors.white,
  },
});

export default styles;
