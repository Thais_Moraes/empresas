import React from 'react';
import { connect } from 'react-redux';
import { FlatList, View } from 'react-native';
import PropTypes from 'prop-types';

import styles from './styles';

import ListItem from '../ListItem';

const List = ({ enterprises }) => (
  <View style={styles.container}>
    <FlatList
      data={enterprises}
      keyExtractor={item => item.id.toString()}
      renderItem={({ item }) => <ListItem key={item.id} item={item} />}
    />
  </View>
);

List.propTypes = {
  enterprises: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
    }),
  ).isRequired,
};

const mapStateToProps = state => ({
  enterprises: state.enterprises.enterpriseList,
});

export default connect(mapStateToProps)(List);
