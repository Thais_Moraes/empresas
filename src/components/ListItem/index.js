import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import PropTypes from 'prop-types';

import { navigate } from '../../services/navigation';
import styles from './styles';

const ListItem = ({ item }) => (
  <View style={styles.outerContainer}>
    <View style={styles.info}>
      <View style={styles.typeContainer}>
        <View style={styles.badge}>
          <Text style={styles.type}>{item.enterprise_type.enterprise_type_name}</Text>
        </View>
      </View>
      <Text style={styles.title}>{item.enterprise_name}</Text>
      <Text style={styles.currency}>{`R$ ${item.share_price.toLocaleString('pt-BR')}`}</Text>
      <Text style={styles.location}>{`${item.city} - ${item.country}`}</Text>
    </View>
    <TouchableOpacity style={styles.details} onPress={() => navigate('Details', { item })}>
      <Text style={styles.detailsText}>+ Detalhes</Text>
    </TouchableOpacity>
  </View>
);

ListItem.propTypes = {
  item: PropTypes.shape({
    enterprise_type: PropTypes.shape({
      enterprise_type_name: PropTypes.string,
    }),
    enterprise_name: PropTypes.string,
    share_price: PropTypes.number,
    city: PropTypes.string,
    country: PropTypes.string,
  }).isRequired,
};

export default ListItem;
