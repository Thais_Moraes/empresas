import { StyleSheet } from 'react-native';
import { colors, metrics } from '../../styles';

const styles = StyleSheet.create({
  outerContainer: {
    flexDirection: 'row',
    backgroundColor: colors.white,
    padding: metrics.basePadding,
    marginBottom: metrics.baseMargin,
    borderRadius: metrics.baseRadius,
  },

  info: {
    flex: 1,
  },

  typeContainer: {
    alignItems: 'flex-start',
  },

  badge: {
    backgroundColor: colors.primary,
    paddingHorizontal: metrics.basePadding / 2,
    borderRadius: metrics.baseRadius * 2,
    marginBottom: metrics.baseMargin,
  },

  type: {
    fontSize: 14,
    color: colors.white,
  },

  titleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: metrics.baseMargin,
  },

  title: {
    fontSize: 16,
    fontWeight: 'bold',
    color: colors.darker,
    marginBottom: metrics.baseMargin,
  },

  currency: {
    fontSize: 18,
    color: colors.dark,
    marginBottom: metrics.baseMargin,
  },

  location: {
    fontSize: 14,
    color: colors.darker,
  },

  details: {
    justifyContent: 'center',
    alignItems: 'center',
  },

  detailsText: {
    color: colors.primary,
  },
});

export default styles;
