import Reactotron from 'reactotron-react-native';
import { reactotronRedux } from 'reactotron-redux';

const tron = __DEV__
  ? Reactotron.configure()
    .useReactNative()
    .use(reactotronRedux())
    .connect()
  : null;

if (__DEV__) {
  tron.clear();
}

export default tron;
