import './config/ReactotronConfig';

import React, { Component } from 'react';
import { AsyncStorage } from 'react-native';
import { Provider } from 'react-redux';
import FlashMessage from 'react-native-flash-message';

import { setNavigator } from './services/navigation';
import createNavigator from './routes';
import store from './store';

export default class App extends Component {
  state = {
    userLogged: false,
  };

  async componentWillMount() {
    const retrievedAuthData = await AsyncStorage.getItem('@Enterprises:authConfig');
    const authData = JSON.parse(retrievedAuthData);

    this.setState({
      userLogged: !!authData,
    });
  }

  render() {
    const { userLogged } = this.state;
    const Routes = createNavigator(userLogged);
    return (
      <Provider store={store}>
        <Routes ref={setNavigator} />
        <FlashMessage position="top" />
      </Provider>
    );
  }
}
