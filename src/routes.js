import { createAppContainer, createSwitchNavigator, createStackNavigator } from 'react-navigation';

import Login from './screens/Login';
import Home from './screens/Home';
import Details from './screens/Details';

const Routes = userIsLogged => createAppContainer(
  createSwitchNavigator(
    {
      Login,
      App: createStackNavigator(
        { Home, Details },
        {
          headerMode: 'none',
        },
      ),
    },
    { initialRouteName: userIsLogged ? 'App' : 'Login' },
  ),
);

export default Routes;
