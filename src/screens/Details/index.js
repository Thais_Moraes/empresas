import React from 'react';
import {
  Image, ScrollView, Text, View,
} from 'react-native';
import { API_URL } from 'react-native-dotenv';
import Icon from 'react-native-vector-icons/FontAwesome5';
import PropTypes from 'prop-types';

import styles from './styles';

const Details = ({ navigation }) => {
  const item = navigation.getParam('item', {});
  return (
    <View style={styles.background}>
      <View style={styles.container}>
        <View style={styles.imageContainer}>
          {item.photo ? (
            <Image
              style={styles.imageStyle}
              source={{
                uri: `${API_URL}${item.photo}`,
              }}
            />
          ) : (
            <View style={styles.iconContainer}>
              <Icon name="image" size={40} style={styles.icon} />
            </View>
          )}
        </View>
        <View style={styles.infoContainer}>
          <ScrollView>
            <Text style={styles.title}>{item.enterprise_name}</Text>
            <Text multiline style={styles.description}>
              {item.description}
            </Text>

            <Text style={styles.currency}>
              {`R$ ${item.share_price.toLocaleString('pt-BR')}`}
              <Text style={styles.bold}> (preço da ação)</Text>
            </Text>
            <Text style={styles.bold}>
              {'Tipo: '}
              <Text style={styles.description}>{item.enterprise_type.enterprise_type_name}</Text>
            </Text>
            <Text style={styles.bold}>
              {'Localização: '}
              <Text style={styles.description}>{`${item.city} - ${item.country}`}</Text>
            </Text>
          </ScrollView>
        </View>
      </View>
    </View>
  );
};

Details.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
  }).isRequired,
};

export default Details;
