import { StyleSheet } from 'react-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { colors, metrics } from '../../styles';

const styles = StyleSheet.create({
  background: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
    backgroundColor: colors.primary,
  },

  container: {
    flex: 1,
    backgroundColor: colors.white,
    borderRadius: metrics.baseRadius * 2,
    margin: metrics.baseMargin * 3,
    marginTop: metrics.baseMargin * 2 + getStatusBarHeight(true),
    marginBottom: metrics.baseMargin * 2 + getStatusBarHeight(true),
  },

  imageContainer: {
    flex: 0.25,
    borderTopLeftRadius: metrics.baseRadius * 2,
    borderTopRightRadius: metrics.baseRadius * 2,
    overflow: 'hidden',
    borderBottomWidth: 1,
    borderBottomColor: colors.light,
  },

  imageStyle: {
    flex: 1,
  },

  iconContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  icon: {
    color: colors.primary,
  },

  infoContainer: {
    flex: 0.75,
    padding: metrics.basePadding,
  },

  title: {
    color: colors.darker,
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: metrics.baseMargin * 2,
    textAlign: 'center',
  },

  description: {
    color: colors.dark,
    fontSize: 16,
    fontWeight: '400',
  },

  currency: {
    fontSize: 24,
    color: colors.darker,
    marginBottom: metrics.baseMargin * 2,
    marginTop: metrics.baseMargin * 2,
  },

  bold: {
    fontWeight: 'bold',
    color: colors.darker,
    fontSize: 16,
  },
});

export default styles;
