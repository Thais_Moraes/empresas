import React, { Component } from 'react';
import {
  AsyncStorage, Text, TextInput, TouchableOpacity, View,
} from 'react-native';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Icon from 'react-native-vector-icons/FontAwesome5';
import PropTypes from 'prop-types';

import * as EnterpriseActions from '../../store/actions/enterprises';

import { colors } from '../../styles';
import Header from '../../components/Header';
import List from '../../components/List';
import CustomPicker from '../../components/CustomPicker';
import styles from './styles';

class Home extends Component {
  static propTypes = {
    loadEnterprisesRequest: PropTypes.func.isRequired,
    types: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number,
        enterprise_type_name: PropTypes.string,
      }),
    ).isRequired,
    authData: PropTypes.shape({
      accessToken: PropTypes.string,
      client: PropTypes.string,
      uid: PropTypes.string,
    }).isRequired,
    signout: PropTypes.bool.isRequired,
  };

  state = {
    enterpriseName: '',
    typeSelected: -1,
  };

  componentWillMount() {
    const { loadEnterprisesRequest } = this.props;
    loadEnterprisesRequest();
  }

  async componentWillUnmount() {
    const { authData, signout } = this.props;

    if (!signout) await AsyncStorage.setItem('@Enterprises:authConfig', JSON.stringify(authData));
  }

  setSelectedValue = (value) => {
    this.setState({ typeSelected: value });
  };

  searchEnterprises = () => {
    const { enterpriseName, typeSelected } = this.state;
    const { loadEnterprisesRequest } = this.props;
    loadEnterprisesRequest(enterpriseName, typeSelected);
    this.setState({ enterpriseName: '' });
  };

  render() {
    const { typeSelected, enterpriseName } = this.state;
    const { types } = this.props;
    return (
      <View style={styles.outerContainer}>
        <Header title="EMPRESAS" />

        <View style={styles.container}>
          <Text style={styles.title}>Buscar empresas</Text>
          <View>
            <View style={styles.searchFields}>
              <TextInput
                style={styles.input}
                value={enterpriseName}
                autoCapitalize="none"
                autoCorrect={false}
                placeholder="Busque por nome"
                underlineColorAndroid="transparent"
                onChangeText={text => this.setState({ enterpriseName: text })}
                placeholderTextColor={colors.dark}
              />
              <CustomPicker
                list={types}
                selectedValue={typeSelected}
                setSelectedValue={this.setSelectedValue}
                description="Selecione o tipo de empresa desejado."
              />
            </View>
            <TouchableOpacity onPress={this.searchEnterprises} style={styles.search}>
              <Icon name="search" size={18} style={styles.icon} />
              <Text style={styles.searchText}>Buscar</Text>
            </TouchableOpacity>
          </View>
        </View>

        <List />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  enterprises: state.enterprises.enterpriseList,
  types: state.types,
  authData: state.auth.authData,
  signout: state.signout.status,
});

const mapDispatchToProps = dispatch => bindActionCreators(EnterpriseActions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Home);
