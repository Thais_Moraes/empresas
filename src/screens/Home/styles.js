import { StyleSheet } from 'react-native';
import { colors, metrics } from '../../styles';

const styles = StyleSheet.create({
  outerContainer: {
    flex: 1,
  },

  container: {
    padding: metrics.basePadding,
    borderBottomWidth: 1,
    borderBottomColor: colors.light,
  },

  title: {
    fontSize: 18,
    color: colors.darker,
    fontWeight: 'bold',
    marginBottom: metrics.baseMargin * 2,
  },

  searchFields: {
    flexDirection: 'row',
    marginBottom: metrics.baseMargin,
  },

  input: {
    flex: 1,
    height: 40,
    backgroundColor: colors.lighter,
    borderRadius: metrics.baseRadius,
    marginRight: metrics.baseMargin,
    paddingHorizontal: metrics.basePadding,
  },

  search: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.primary,
    borderRadius: metrics.baseRadius,
    height: 40,
    paddingHorizontal: metrics.basePadding,
  },

  icon: {
    color: colors.white,
    marginRight: metrics.baseMargin,
  },

  searchText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: colors.white,
  },
});

export default styles;
