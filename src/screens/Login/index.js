import React, { Component } from 'react';
import {
  Text, TextInput, TouchableOpacity, View, ActivityIndicator,
} from 'react-native';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Icon from 'react-native-vector-icons/FontAwesome5';
import * as AuthActions from '../../store/actions/auth';

import styles from './styles';

class Login extends Component {
  state = {
    email: '',
    password: '',
  };

  signIn = () => {
    const { email, password } = this.state;
    const { signInRequest } = this.props;

    signInRequest(email, password);
  };

  render() {
    const { email } = this.state;
    const { loading } = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.iconContainer}>
          <Icon style={styles.icon} name="handshake" />
        </View>
        <Text style={styles.title}>EMPRESAS</Text>

        <View style={styles.form}>
          <TextInput
            style={styles.input}
            value={email}
            autoCapitalize="none"
            autoCorrect={false}
            placeholder="E-mail"
            underlineColorAndroid="transparent"
            onChangeText={text => this.setState({ email: text.trim() })}
          />

          <TextInput
            style={styles.input}
            autoCapitalize="none"
            autoCorrect={false}
            placeholder="Senha"
            underlineColorAndroid="transparent"
            onChangeText={text => this.setState({ password: text })}
            secureTextEntry
          />

          <TouchableOpacity style={styles.button} onPress={this.signIn}>
            {loading ? (
              <ActivityIndicator size="small" color="#fff" />
            ) : (
              <Text style={styles.buttonText}>Entrar</Text>
            )}
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  loading: state.auth.loading,
});

const mapDispatchToProps = dispatch => bindActionCreators(AuthActions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Login);
