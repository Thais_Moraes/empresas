import { StyleSheet } from 'react-native';
import { colors, metrics } from '../../styles';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
    padding: metrics.basePadding * 2,
    backgroundColor: colors.prim,
  },

  iconContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: metrics.basePadding * 2,
  },

  icon: {
    fontSize: 72,
    color: colors.primary,
  },

  title: {
    textAlign: 'center',
    color: colors.primary,
    fontSize: 36,
    fontWeight: '200',
  },

  form: {
    marginTop: metrics.baseMargin * 4,
  },

  input: {
    backgroundColor: colors.lighter,
    borderRadius: metrics.baseRadius,
    height: 44,
    paddingHorizontal: metrics.basePadding,
    marginBottom: metrics.baseMargin,
  },

  button: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.primary,
    borderRadius: metrics.baseRadius,
    height: 44,
    paddingHorizontal: metrics.basePadding,
    marginTop: metrics.baseMargin * 2,
  },

  buttonText: {
    color: colors.white,
    fontWeight: 'bold',
    fontSize: 15,
  },
});

export default styles;
