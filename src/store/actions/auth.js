export const signInRequest = (email, password) => ({
  type: 'SIGNIN_REQUEST',
  payload: { email, password },
});

export const signInSuccess = authData => ({
  type: 'SIGNIN_SUCCESS',
  payload: { authData },
});

export const signInFailure = () => ({
  type: 'SIGNIN_FAILURE',
});
