export const loadEnterprisesRequest = (name = '', type = -1) => ({
  type: 'LOAD_ENTERPRISES_REQUEST',
  payload: { name, type },
});

export const loadEnterprisesSuccess = data => ({
  type: 'LOAD_ENTERPRISES_SUCCESS',
  payload: { data },
});

export const loadEnterprisesFailure = () => ({
  type: 'LOAD_ENTERPRISES_FAILURE',
});
