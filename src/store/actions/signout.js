export const signOutRequest = () => ({
  type: 'SIGNOUT_REQUEST',
});

export const signOutFailure = () => ({
  type: 'SIGNOUT_FAILURE',
});
