export const loadTypesSuccess = types => ({
  type: 'LOAD_TYPES_SUCCESS',
  payload: { types },
});

export const loadTypesFailure = () => ({
  type: 'LOAD_TYPES_FAILURE',
});
