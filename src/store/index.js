import { createStore, compose, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';

import Reactotron from '../config/ReactotronConfig';
import rootReducer from './reducers';
import rootSaga from './sagas';

const middlewares = [];

const sagaMonitor = __DEV__ ? Reactotron.createSagaMonitor : null;
const sagaMiddleware = createSagaMiddleware({ sagaMonitor });

middlewares.push(sagaMiddleware);

const composer = __DEV__
  ? compose(
    applyMiddleware(...middlewares),
    Reactotron.createEnhancer(),
  )
  : compose(applyMiddleware(...middlewares));

const store = createStore(rootReducer, composer);

sagaMiddleware.run(rootSaga);

export default store;
