const INITIAL_STATE = {
  authData: {
    accessToken: null,
    client: null,
    uid: null,
  },
  loading: false,
};

export default function auth(state = INITIAL_STATE, action) {
  switch (action.type) {
    case 'SIGNIN_REQUEST':
      return { ...state, loading: true };
    case 'SIGNIN_SUCCESS':
      return { ...state, authData: action.payload.authData, loading: false };
    case 'SIGNIN_FAILURE':
      return { ...state, loading: false };
    case 'SIGNOUT':
      return INITIAL_STATE;
    default:
      return state;
  }
}
