const INITIAL_STATE = {
  enterpriseList: [],
  loading: false,
};

export default function enterprises(state = INITIAL_STATE, action) {
  switch (action.type) {
    case 'LOAD_ENTERPRISES_REQUEST':
      return { ...state, loading: true };
    case 'LOAD_ENTERPRISES_SUCCESS':
      return { ...state, enterpriseList: action.payload.data.enterprises, loading: false };
    case 'LOAD_ENTERPRISES_FAILURE':
      return { ...state, loading: false };
    case 'SIGNOUT':
      return INITIAL_STATE;
    default:
      return state;
  }
}
