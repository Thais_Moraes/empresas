import { combineReducers } from 'redux';

import auth from './auth';
import enterprises from './enterprises';
import types from './types';
import signout from './signout';

export default combineReducers({
  auth,
  enterprises,
  types,
  signout,
});
