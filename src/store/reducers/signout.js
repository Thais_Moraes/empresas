const INITIAL_STATE = {
  status: false,
};

export default function enterprises(state = INITIAL_STATE, action) {
  switch (action.type) {
    case 'SIGNIN_SUCCESS':
      return { status: false };
    case 'SIGNOUT_REQUEST':
      return { status: true };
    default:
      return state;
  }
}
