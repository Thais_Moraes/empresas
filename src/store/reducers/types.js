const INITIAL_STATE = [
  {
    id: -1,
    enterprise_type_name: 'Todos os tipos',
  },
];

export default function enterprises(state = INITIAL_STATE, action) {
  switch (action.type) {
    case 'LOAD_TYPES_SUCCESS':
      return [...state, ...action.payload.types];
    case 'LOAD_ENTERPRISES_FAILURE':
      return state;
    case 'SIGNOUT':
      return INITIAL_STATE;
    default:
      return state;
  }
}
