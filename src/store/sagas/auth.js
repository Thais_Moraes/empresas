import { call, put } from 'redux-saga/effects';
import { AsyncStorage } from 'react-native';
import { showMessage } from 'react-native-flash-message';

import api from '../../services/api';
import * as AuthActions from '../actions/auth';
import { navigate } from '../../services/navigation';

const storeConfig = async (authConfig) => {
  try {
    await AsyncStorage.setItem('@Enterprises:authConfig', JSON.stringify(authConfig));
  } catch (error) {
    showMessage({
      message: 'Erro ao salvar credenciais.',
      type: 'danger',
    });
  }
};

export default function* auth(action) {
  try {
    const { email, password } = action.payload;

    const { headers } = yield call(api.post, 'users/auth/sign_in', { email, password });

    const authConfig = {
      accessToken: headers['access-token'],
      client: headers.client,
      uid: headers.uid,
    };

    yield call(storeConfig, authConfig);

    yield put(AuthActions.signInSuccess(authConfig));

    navigate('Home');
  } catch (error) {
    if (error.response.status === 401) {
      showMessage({
        message: 'Credenciais inválidas!',
        type: 'danger',
      });
    }
    yield put(AuthActions.signInFailure());
  }
}
