import { call, put, select } from 'redux-saga/effects';
import { AsyncStorage } from 'react-native';
import { showMessage } from 'react-native-flash-message';

import { navigate } from '../../services/navigation';
import api from '../../services/api';
import * as EnterpriseActions from '../actions/enterprises';

const getAuthHeaders = state => state.auth.authData;

const getUrlParams = (name, type) => {
  let params;

  if (!(name === '') && !(type === -1)) {
    params = `?enterprise_types=${type}&name=${name}`;
  } else if (!(name === '')) {
    params = `?name=${name}`;
  } else if (!(type === -1)) {
    params = `?enterprise_types=${type}`;
  } else {
    params = '';
  }

  return params;
};

const getStoredConfig = async () => {
  try {
    const retrievedAuthData = await AsyncStorage.getItem('@Enterprises:authConfig');
    const authHeaders = JSON.parse(retrievedAuthData);

    return authHeaders;
  } catch (error) {
    return null;
  }
};

export default function* auth(action) {
  const { name, type } = action.payload;
  try {
    let authHeaders = yield select(getAuthHeaders);

    if (!authHeaders.uid) {
      authHeaders = yield getStoredConfig();
    }

    const requestConfig = {
      headers: {
        'access-token': authHeaders.accessToken,
        client: authHeaders.client,
        uid: authHeaders.uid,
      },
    };

    const queryParams = getUrlParams(name, type);

    const { data } = yield call(api.get, `enterprises${queryParams}`, requestConfig);
    yield put(EnterpriseActions.loadEnterprisesSuccess(data));
  } catch (error) {
    if (error.response.status === 401) {
      navigate('Login');
      showMessage({
        message: 'Credenciais expiradas. Entre novamente.',
        type: 'danger',
      });
    }
    showMessage({
      message: 'Erro ao carregar listagem de empresas.',
      type: 'danger',
    });
    yield put(EnterpriseActions.loadEnterprisesFailure());
  }
}
