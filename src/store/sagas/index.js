import { all, takeLatest } from 'redux-saga/effects';

import auth from './auth';
import enterprises from './enterprises';
import types from './types';
import signout from './signout';

export default function* rootSaga() {
  return yield all([
    takeLatest('SIGNIN_REQUEST', auth),
    takeLatest('LOAD_ENTERPRISES_REQUEST', enterprises),
    takeLatest('LOAD_ENTERPRISES_SUCCESS', types),
    takeLatest('SIGNOUT_REQUEST', signout),
  ]);
}
