import { put } from 'redux-saga/effects';
import { AsyncStorage } from 'react-native';
import { showMessage } from 'react-native-flash-message';

import { navigate } from '../../services/navigation';
import * as SignOutActions from '../actions/signout';

const clearStorage = async () => {
  try {
    await AsyncStorage.clear();
  } catch (error) {
    showMessage({
      message: 'Erro ao limpar credenciais.',
      type: 'danger',
    });
  }
};

export default function* auth() {
  try {
    yield clearStorage();

    navigate('Login');
  } catch (error) {
    yield put(SignOutActions.signOutFailure());
  }
}
