import { put, select } from 'redux-saga/effects';
import { showMessage } from 'react-native-flash-message';

import { navigate } from '../../services/navigation';
import * as TypeActions from '../actions/types';

const getEnterpriseList = state => state.enterprises.enterpriseList;

export default function* types() {
  try {
    const enterprises = yield select(getEnterpriseList);

    // Get enterprise types from enterprises list
    const enterpriseTypes = enterprises.map(enterprise => enterprise.enterprise_type);

    // Remove duplicates
    const filteredTypes = enterpriseTypes.filter(
      (enterprise, index, self) => index
        === self.findIndex(
          t => t.id === enterprise.id && t.enterprise_type_name === enterprise.enterprise_type_name,
        ),
    );

    // Order types by id
    const sortedTypes = [...filteredTypes].sort((a, b) => a.id - b.id);

    yield put(TypeActions.loadTypesSuccess(sortedTypes));
  } catch (error) {
    if (error.response.status === 401) {
      navigate('Login');
      showMessage({
        message: 'Credenciais expiradas. Entre novamente.',
        type: 'danger',
      });
    }
    showMessage({
      message: 'Erro ao carregar tipos de empresas.',
      type: 'danger',
    });
    yield put(TypeActions.loadTypesFailure());
  }
}
